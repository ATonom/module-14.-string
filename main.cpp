﻿#include <iostream>
#include <string>

int main()
{
    std::string name{ "Sergey" };

    std::cout << "name - " << name << std::endl;
    std::cout << "String length - " << name.length() << std::endl;
    std::cout << "First character - " << name[0] << std::endl;
    std::cout << "Last character - " << name[name.length() - 1] << std::endl;

    return 0;
}